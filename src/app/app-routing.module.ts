import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CreateTradeComponent} from './component/create-trade/create-trade.component';
import {HoldingComponent} from './holding/holding.component';
import {TradeComponent} from './trade/trade.component'
import { AuthenticationComponent } from './authentication/authentication.component';
import { SignupComponent } from './signup/signup.component';
import { InformationComponent } from './information/information.component';

const routes: Routes = [
  {path: 'createTrade', component: CreateTradeComponent},
  {path: 'getHolding', component: HoldingComponent},
  {path: 'getHistory', component: TradeComponent},
  {path: 'getInformation', component: InformationComponent},
  {path: 'login', component: AuthenticationComponent},
  {path: 'signup', component: SignupComponent},
  {path: '', redirectTo: 'getHolding', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
