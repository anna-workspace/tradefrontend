import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';
import { unwrapResolvedMetadata } from '@angular/compiler';
import { InformationItem } from './informationitem';
import { json } from './data';


// TODO: replace this with real data from your application
const EXAMPLE_DATA: InformationItem[] = [
  {ticker: 'A',  date: '18 Sep 2020', name: 'AGILENT TECHNOLOGIES INC', currentPrice: "123"},
  {ticker: 'AA',  date: '18 Sep 2020', name: 'ALCOA CORP', currentPrice: "23"},
  {ticker: 'AAAU',  date: '18 Sep 2020', name: 'PERTH MINT PHYSICAL GOLD ETF', currentPrice: "83"}
];

// attempted to view directly, however, the table plotting broke as a result.
const jsondata : InformationItem[] = json;

/**
 * Data source for the Information view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class InformationDataSource extends DataSource<InformationItem> {
  data: InformationItem[] = json;
  paginator: MatPaginator;
  sort: MatSort;

  constructor() {
    super();
  }


  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<InformationItem[]> {
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.

    const dataMutations = [
      observableOf(this.data),
      this.paginator.page,
      this.sort.sortChange
    ];

    return merge(...dataMutations).pipe(map(() => {
      return this.getPagedData(this.getSortedData([...this.data]));
    }));
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {}

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: InformationItem[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: InformationItem[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'ticker': return compare(a.ticker, b.ticker, isAsc);
        case 'price': return compare(+a.currentPrice, +b.currentPrice, isAsc);
        default: return 0;
      }
    });
  }

}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a: string | number, b: string | number, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
