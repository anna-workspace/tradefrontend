import { Component, OnInit } from '@angular/core';
import { User } from '../model/user';
import { AuthenticationService } from '../authentication-service/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {

  public error : boolean = false;
  public success : boolean = false;

  public errorMessage;

  constructor(private as : AuthenticationService, private router : Router) { }

  signup(name : string, email : string, password : string, phone : string) {
    let user = new User(name, email, password, Number(phone));
    this.as.signup(user, () => {
      this.router.navigateByUrl('/');
      this.success = true;
    })
    this.error = !this.success;
    this.errorMessage = this.as.errorMessage;
  }


}
