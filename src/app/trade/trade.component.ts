import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { GetTradesService } from '../get-trades/get-trades.service';
import { Observable, BehaviorSubject, fromEvent, pipe } from 'rxjs';
import { Trade } from '../model/trade'
import { FetcherService } from '../price-fetcher/fetcher.service';
import { TradeService } from '../service/trade.service'
import { MatPaginator } from '@angular/material/paginator';
import { MatSpinner } from '@angular/material/progress-spinner'
import {MatSelectModule } from '@angular/material/select';
import { MatInput } from '@angular/material/input'
import { Quote } from '../price-fetcher/quote';
import { MatSort } from '@angular/material/sort';

interface Ticker{
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-trade',
  templateUrl: './trade.component.html',
  styleUrls: ['./trade.component.css']
})

export class TradeComponent {

  public trades$: Observable<Array<Trade>>;
  public tickers$: Observable<Array<String>>;
  isLoading$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  displayedColumns: string[] = ['ticker', 'companyName', 'unitsPurchased', 'unitPrice', 'totalPrice'];
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatInput) input: ElementRef;
  
  constructor(private getTradesService : GetTradesService, private fetcher : FetcherService) {}

  getPrice(ticker : string) : Observable<Quote> {
    return this.fetcher.fetch(ticker);
  }
  
  tickers: Ticker[] = [
    {value: 'None', viewValue: 'None'},
    {value: 'FB', viewValue: 'Facebook Inc.'},
    {value: 'C', viewValue: 'Citigroup Inc'},
    {value: 'MSFT', viewValue: 'Microsoft Corporation'}
  ]

  ngOnInit(): void {

    this.trades$ = this.getTradesService.getHistory();
    // this.tickers$ = this.getTradesService.getTickers();
  }


}
